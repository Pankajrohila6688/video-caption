import axios from 'axios';

const KEY = 'AIzaSyBmSiSZH6mkQnzlNT8sNuWpn-5LqN5SjQQ';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 5,
    key: KEY,
  },
});