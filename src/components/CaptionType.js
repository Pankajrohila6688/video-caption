import React from 'react';
import '../styles/CaptionType.css';
import HelpOutlineOutlinedIcon from '@mui/icons-material/HelpOutlineOutlined';

function CaptionType({ icon, text, click }) {
    return (
        <div onClick={click} className="captionType">
            <div className="captionType__content">
                <icon style={{ color: 'white' }}>{icon}</icon>
                <p>{text}</p>
            </div>

            <div>
                <HelpOutlineOutlinedIcon style={{ color: 'white' }} />
            </div>
        </div>
    )
}

export default CaptionType;
