import React from 'react';
import { useState } from 'react';
import '../styles/Audio.css';
import SubtitlesIcon from '@mui/icons-material/Subtitles';
import MusicNoteIcon from '@mui/icons-material/MusicNote';

function Audio() {

    const [volume, setVolume] = useState(1);

    return (
        <div className="audio">
            <div className="audio__header">
                <div className="audio__headerLeft">
                    <p className="activep">0.03.28</p>
                    <p>UNDO</p>
                    <p>REDO</p>
                </div>
                <div className="audio__headerRight">
                    <input
                        type="range"
                        min={0}
                        max={1}
                        step={0.02}
                        value={volume}
                        onChange={event => {
                            setVolume(event.target.valueAsNumber)
                        }}
                    />
                </div>
            </div>

            <div className="audio__content">
                <div className="audio__contentTop">
                    <div style={{ width: '10%', borderRight: '1px solid gray', padding: '10px' }}>
                        <SubtitlesIcon className="audio__icons" />
                    </div>
                </div>
                <div className="audio__contentBottom">
                    <div style={{ width: '10%', borderRight: '1px solid gray', padding: '10px' }}>
                        <MusicNoteIcon className="audio__icons" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Audio
