import React, { useEffect, useState } from 'react';
import '../styles/Video.css';
import Checkbox from '@mui/material/Checkbox';
import VideoDetail from './VideoDetail';
import useVideos from '../hooks/useVideos';

function Video() {

    const [selectedVideo, setSelectedVideo] = useState(null);
    const [videos] = useVideos('web3.0');

    useEffect(() => {
        setSelectedVideo(videos[0]);
    }, [videos]);

    return (
        <div className="video">
            <div className="video__container">
                <VideoDetail video={selectedVideo} />
            </div>

            <h5>Enter subtitles faster with <span style={{ color: 'skyblue' }}>keyboard shortcuts</span></h5>

            <div className="video__checkbox">
                <Checkbox style={{ color: 'white' }} />
                <h4>Pause while typing</h4>
            </div>
            
        </div>
    )
}

export default Video;
