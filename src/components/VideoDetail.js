import React from 'react';

const VideoDetail = ({ video }) => {
    if (!video) {
        return <div style={{ color: 'white', display: 'flex', justifyContent: 'center' }}>Loading...</div>
    }

    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`

    return (
        <div style={{ padding: '5px' }}>
            <iframe style={{ width: '100%', height: '45vh', border: 'none' }} title="video player" src={videoSrc} />
        </div>
    )
};

export default VideoDetail;