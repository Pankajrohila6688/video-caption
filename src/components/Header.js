import React from 'react';
import '../styles/Header.css';
import SubtitlesIcon from '@mui/icons-material/Subtitles';
import ChatBubbleIcon from '@mui/icons-material/ChatBubble';
import CloseIcon from '@mui/icons-material/Close';

function Header() {
    return (
        <div className="header">
            <div className="header__left">
                <SubtitlesIcon className="subtitleIcon" />
                <h2>English(India)</h2>
            </div>

            <div className="header__right">
                <ChatBubbleIcon />
                <p>SAVE DRAFT</p>
                <p className="active">PUBLISH</p>
                <CloseIcon />
            </div>
        </div>
    )
}

export default Header;
