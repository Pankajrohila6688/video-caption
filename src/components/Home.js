import React from 'react';
import '../styles/Home.css';
import Audio from './Audio';
import CaptionFeature from './CaptionFeature';
import Video from './Video';

function Home() {
    return (
        <div className="home">
            <div className="home__container">
                <div className="home__left">
                    <CaptionFeature />
                </div>
                <div className="home__right">
                    <Video />
                </div>
            </div>

            <Audio />
        </div>
    )
}

export default Home;
