import React from 'react';
import '../styles/CaptionFeature.css';
import CaptionType from './CaptionType';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import AutoAwesomeIcon from '@mui/icons-material/AutoAwesome';
import KeyboardIcon from '@mui/icons-material/Keyboard';
import TranslateIcon from '@mui/icons-material/Translate';
import { useState } from 'react';
import AddCaption from './AddCaption';

function CaptionFeature() {

    const [items, setItems] = useState(false);

    return (
        <div className="captionFeature">

            {!items ? (
                <div className="captionFeature__content">
                    <h4>Select how you want to add captions</h4>
                    <CaptionType icon={<FileUploadIcon />} text="Upload file" />
                    <CaptionType icon={<AutoAwesomeIcon />} text="Auto-sync" />
                    <CaptionType click={() => setItems(true)} icon={<KeyboardIcon />} text="Type manually" />
                    <CaptionType icon={<TranslateIcon />} text="Auto-translate" />
                </div>
            ) : (
                <div>
                    <AddCaption />
                </div>
            )}

        </div>
    )
}

export default CaptionFeature;
