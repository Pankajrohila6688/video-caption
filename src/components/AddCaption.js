import React from 'react';
import '../styles/AddCaption.css';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

function AddCaption() {

    const [caption, setCaption] = useState([{ value: null }]);

    function handleAdd() {
        const values1 = [...caption];
        values1.push({ value: null });
        setCaption(values1);
    }

    function handleRemove(i) {
        const values1 = [...caption];
        values1.splice(i, 1);
        setCaption(values1);
    }

    return (
        <div className="addCaption">
            <div className="addCaption__header">
                <div onClick={() => handleAdd()} className="addCaption__headerLeft">
                    <AddOutlinedIcon />
                    <p>CAPTION</p>
                </div>

                <div className="addCaption__headerRight">
                    <p>EDIT AS TEXT</p>
                    <MoreVertOutlinedIcon style={{ color: 'white' }} />
                </div>
            </div>

            {caption.map((field, idx) => {
                return (                
                    <div key={`${field}-${idx}`} className="addCaption__container">
                        <textarea placeholder="caption" />
                        <div style={{ marginTop: '38px', color: 'white', cursor: 'pointer' }} onClick={() => handleRemove(idx)}><DeleteIcon /></div>
                        <div className="addCaption__ptag">
                            <p>0:00:00</p>
                            <p>0:01:28</p>
                        </div>
                    </div>
                );
            })}
        </div>
    )
}

export default AddCaption;
